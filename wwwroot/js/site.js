﻿// Javascript code.
var canvas = document.querySelector('canvas');

//canvas.width = window.innerWidth;
//canvas.height = window.innerHeight;
var c = canvas.getContext('2d');
//var d = canvas.getContext('2d');
//c.fillRect(100, 100, 100, 100);

console.log(canvas);

function redClick(checkboxElem) {
    if (checkboxElem.checked) {
            var canvas = document.getElementById('myCanvas');
            var ctx = canvas.getContext('2d');
            var painting = document.getElementById('paint');
            var paint_style = getComputedStyle(painting);
                     
            var mouse = {x: 0, y: 0};
            
            canvas.addEventListener('mousemove', function(e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
            }, false);
            
            ctx.lineWidth = 8;
            
            canvas.addEventListener('mousedown', function(e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);
            
            canvas.addEventListener('mousemove', onPaint, false);
            }, false);
            
            canvas.addEventListener('mouseup', function() {
                canvas.removeEventListener('mousemove', onPaint, false);
            }, false);
            
            var onPaint = function() {
                ctx.lineTo(mouse.x, mouse.y);
                ctx.strokeStyle = "red";
                ctx.stroke();
            };
    }
}

function orangeClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "orange";
            ctx.stroke();
        };
    }
}

function yellowClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "yellow";
            ctx.stroke();
        };
    }
}

function greenClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "green";
            ctx.stroke();
        };
    }
}

function blueClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "blue";
            ctx.stroke();
        };
    }
}

function pinkClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "pink";
            ctx.stroke();
        };
    }
}

function purpleClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "purple";
            ctx.stroke();
        };
    }
}

function blackClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "black";
            ctx.stroke();
        };
    }
}

function whiteClick(checkboxElem) {
    if (checkboxElem.checked) {
        var canvas = document.getElementById('myCanvas');
        var ctx = canvas.getContext('2d');
        var painting = document.getElementById('paint');
        var paint_style = getComputedStyle(painting);
        
        var mouse = { x: 0, y: 0 };

        canvas.addEventListener('mousemove', function (e) {
            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);

        ctx.lineWidth = 8;

        canvas.addEventListener('mousedown', function (e) {
            ctx.beginPath();
            ctx.moveTo(mouse.x, mouse.y);

            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function () {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        var onPaint = function () {
            ctx.lineTo(mouse.x, mouse.y);
            ctx.strokeStyle = "white";
            ctx.stroke();
        };
    }
}